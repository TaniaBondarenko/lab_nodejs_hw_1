const fs = require('fs');
const path = require('path');
const regex = /^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/;
const express = require('express');
const app = express();

app.use(express.json())

app.use((req, res, next) => {
    console.log(req.url);
    next();
});

app.post("/api/files", (req, res) => {
    console.log(req.body.filename)
    try {
        if (regex.test(req.body.filename)) {
            fs.writeFile(`api/files/${req.body.filename}`, req.body.content, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    res.status(200).send({ 'message': 'File created successfully' })
                }});
        } else {
            throw new Error();
        }
    }
    catch (error) {
        res.status(400).send({ 'message': 'Please specify content parametr' });
    }
});

//show list of files in direcrory 'files'
app.get('/api/files', (req, res) => {
    try {
        fs.readdir(path.join(__dirname, '/api/files/'), 'utf-8', (err, content) => {
            res.json({
                message: "Successful", files: content
            });
        });
    } catch (error) {
            res.status(400).json({ message: "Client error" });
        } 
});

//read file content
app.get('/api/files/:fileName', (req, res) => {
    const { fileName } = req.params;
    try {
        fs.readFile(path.join(__dirname, '/api/files/', fileName), 'utf-8', (err, content) => {
            if (err) {
               res.status(400).json({ message: `No file with '${fileName}' filename found` })
            } else {
                res.json({
                    message: "Success",
                    filename: fileName,
                    content: content,
                    extension: (path.extname(fileName).slice(1)),
                    uploadedDate: fs.statSync(path.join(__dirname, '/api/files/', fileName)).birthtime,
                });
            }
            });
    } catch (error) {
        console.log(error);
    }
});

app.listen(8080);
